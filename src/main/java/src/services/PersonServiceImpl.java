package src.services;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import src.model.Person;
import src.model.Response;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@Path("/persons")
//@Consumes(MediaType.APPLICATION_JSON)
//@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.TEXT_PLAIN)
@Produces(MediaType.APPLICATION_JSON)
public class PersonServiceImpl implements PersonService {

    private static Map<Integer,Person> persons = new HashMap<Integer, Person>();

    @Override
    @POST
    public String addPerson(String p) throws IOException {
        byte[] jsonData = p.getBytes();
        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode rootNode = objectMapper.readTree(jsonData);
        int id = rootNode.path("id").asInt();
        String name = rootNode.path("name").asText();
        int age = rootNode.path("age").asInt();
        Person person = new Person(id, name, age);

        Response response = new Response();
        if(persons.get(person.getId()) != null){
            response.setStatus(false);
            response.setMessage("Person Already Exists");
            return objectMapper.writeValueAsString(response);
        }
        persons.put(person.getId(), person);
        response.setStatus(true);
        response.setMessage("Person created successfully");
        return objectMapper.writeValueAsString(response);
    }


    @Override
    @DELETE
    @Path("/{id}")
    public Response deletePerson(@PathParam("id") int id) {
        Response response = new Response();
        if(persons.get(id) == null){
            response.setStatus(false);
            response.setMessage("Person Doesn't Exists");
            return response;
        }
        persons.remove(id);
        response.setStatus(true);
        response.setMessage("Person deleted successfully");
        return response;
    }

    @Override
    @GET
    @Path("/{id}")
    public Person getPerson(@PathParam("id") int id) {
        return persons.get(id);
    }


    @Override
    @GET
    public Person[] getAllPersons() {
        Set<Integer> ids = persons.keySet();
        Person[] p = new Person[ids.size()];
        int i=0;
        for(Integer id : ids){
            p[i] = persons.get(id);
            i++;
        }
        return p;
    }

}
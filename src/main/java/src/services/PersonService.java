package src.services;

import src.model.Person;
import src.model.Response;

import java.io.IOException;

public interface PersonService {

    public String addPerson(String p) throws IOException;

    public Response deletePerson(int id);

    public Person getPerson(int id);

    public Person[] getAllPersons();

}
